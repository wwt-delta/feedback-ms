﻿using log4net;
using log4net.Core;
using log4net.Layout.Pattern;
using System;

namespace WWT.Feedback.Services
{
    public class LoggerService<T> : ILoggerService<T>, IDisposable where T : class
    {
        private readonly ILog _logger;
        private bool disposed;

        public LoggerService()
        {
            _logger = LogManager.GetLogger(typeof(T));

        }

        public void Debug(object message)
        {
            _logger.Debug(message);
        }

        public void Error(object message)
        {
            _logger.Error(message);
        }

        public void Error(object message, Exception ex)
        {
            _logger.Error(message, ex);
        }

        public void Error(Exception exception)
        {
            _logger.Error(exception.GetBaseException().Message, exception);
        }

        public void Error(string customMessage, Exception exception)
        {
            _logger.Error(customMessage, exception);
        }

        public void Error(string format, Exception exception, params object[] args)
        {

            _logger.Error(string.Format(format, args), exception);
        }

        public void Warn(Exception exception)
        {
            _logger.Warn(exception.GetBaseException().Message, exception);
        }

        public void Fatal(string message)
        {
            _logger.Fatal(message);
        }

        public void Info(string message)
        {
            _logger.Info(message);
        }

        public void Warn(object message)
        {
            _logger.Warn(message);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    //this.Dispose();
                }
                disposed = true;
            }
        }
    }

   }
