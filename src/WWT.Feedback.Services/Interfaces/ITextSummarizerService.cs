﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WWT.Feedback.Models;

namespace WWT.Feedback.Services.Interfaces
{
    public interface ITextSummarizerService : IDisposable
    {
         FeedbackSummary Summarize(string textSummary);
    }
}
