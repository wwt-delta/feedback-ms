﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using System;
using WWT.Feedback.Services;

namespace WWT.Feedback.API.Helpers
{
    /// <summary>
    /// Global Filter to log the Tracing information
    /// </summary>
    /// <seealso cref="ActionFilterAttribute" />
    public sealed class LoggingFilterAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// Gets or sets the logger.
        /// </summary>
        /// <value>
        /// The logger.
        /// </value>
        [Dependency]
        public ILoggerService<LoggingFilterAttribute> logger { get; set; }

        /// <summary>
        /// Called when [action executed].
        /// </summary>
        /// <param name="actionContext">The action context.</param>
        public override void OnActionExecuted(ActionExecutedContext actionContext)
        {
            var controllerNamespace = string.Format("Trace - {0}.{1} {2}", actionContext.Controller.GetType().Name, actionContext.ActionDescriptor.DisplayName, "Response");

            if (actionContext.HttpContext.Request != null && actionContext.Exception == null)
            {
                var reponseObject = new
                {
                    Response = new
                    {
                        StatusCode = actionContext.HttpContext.Response.StatusCode,
                        // StatusDescription = actionContext.HttpContext.Response.c,
                        Exception = actionContext.Exception
                    }
                };
                logger.Info(string.Format("Path: {0}{1} Response Details:{2}", controllerNamespace, Environment.NewLine, JsonConvert.SerializeObject(reponseObject)));

            }
            else
            {
                logger.Info(string.Format("Path: {0} - Exception Occurred.", controllerNamespace));
            }
            base.OnActionExecuted(actionContext);
        }

        /// <summary>
        /// Occurs before the action method is invoked.
        /// </summary>
        /// <param name="actionContext">The action context.</param>
        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            logger = new LoggerService<LoggingFilterAttribute>();
            var controllerNamespace = string.Format("Trace - {0} {1}.{2} {3}",
                actionContext.HttpContext.Request.Method,
                actionContext.Controller.GetType().Name,
                actionContext.ActionDescriptor.DisplayName, "Request");
            var requestObject = new
            {
                Request = new
                {
                    Parameters = JsonConvert.SerializeObject(actionContext.ActionArguments),
                    Url = actionContext.HttpContext.Request.Path,
                    Headers = JsonConvert.SerializeObject(actionContext.HttpContext.Request.Headers)
                }
            };

            logger.Info(string.Format("Path: {0}{1}Request Details:{2}", controllerNamespace, Environment.NewLine, JsonConvert.SerializeObject(requestObject)));

            base.OnActionExecuting(actionContext);
        }
    }
}
