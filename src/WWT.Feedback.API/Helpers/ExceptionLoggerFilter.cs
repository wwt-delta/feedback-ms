﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Practices.Unity;
using System;
using WWT.Feedback.Services;
namespace WWT.Feedback.API.Helpers
{
    /// <summary>
    /// To log the exception details
    /// </summary>
    public sealed class ExceptionLoggerFilter : ExceptionFilterAttribute
    {
        /// <summary>
        /// Logger Object
        /// </summary>
        [Dependency]
        public ILoggerService<ExceptionLoggerFilter> _logger { get; set; }


        /// <summary>
        /// Called when [exception].
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public override void OnException(ExceptionContext filterContext)
        {

            if (filterContext.Exception != null)
            {
                _logger = new LoggerService<ExceptionLoggerFilter>();
                _logger.Error(String.Format("Controller:{0}|Action:{1}",
                    filterContext.HttpContext.GetType().FullName,
                    filterContext.ActionDescriptor.DisplayName),
                    filterContext.Exception);
            }

        }

    }
}
