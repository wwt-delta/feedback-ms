﻿namespace WWT.Feedback.API.Constants
{
    internal static class ErrorMessages
    {
        public static string FeedbackText { get { return "Feedback Text is required."; } }

        public static string Version { get { return "Version number is required."; } }
    }
}
