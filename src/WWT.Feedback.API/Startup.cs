﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Steeltoe.Extensions.Configuration;
using Swashbuckle.Swagger.Model;
using WWT.Feedback.Models;
using WWT.Feedback.Services;
using WWT.Feedback.Services.Interfaces;
using WWT.Feedback.API.Helpers;

namespace WWT.Feedback.API
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsEnvironment("Development"))
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);

            //Adding CORS
            services.AddCors();

            //Adding Global Filters for Auto Logging and Exception Handling
            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(LoggingFilterAttribute));
                options.Filters.Add(new ExceptionLoggerFilter());

            });

            services.AddSwaggerGen();
            services.ConfigureSwaggerGen(options =>
            {
                options.SingleApiVersion(new Info
                {
                    Version = "v1",
                    Title = "Feedback.API",
                });
                options.IncludeXmlComments(GetXmlCommentsPath());
                options.DescribeAllEnumsAsStrings();
            });
            Console.Write(AppContext.BaseDirectory);

            #region "Register Dependency Services"

            services.AddSingleton<IConfigurationRoot>(Configuration);

            //Get Service provider Details from Configuration.
            var configurationSection = Configuration.GetSection("AppSettings");
            var baseURL = configurationSection.GetValue<string>("baseURL");
            var securityKey = configurationSection.GetValue<string>("securityKey");
            var securityValue = configurationSection.GetValue<string>("securityValue");
            var wordCount = configurationSection.GetValue<int>("wordCount");


            //Register a service factory
            Func<IServiceProvider, ITextSummarizerService> _textSummarizeService = provider => new TextSummarizerService(baseURL, securityKey, securityValue, wordCount);
            services.AddTransient(_textSummarizeService);
            services.AddSingleton(typeof(ILoggerService<>), typeof(LoggerService<>));

            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            //loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            //loggerFactory.AddDebug();

            app.UseApplicationInsightsRequestTelemetry();

            app.UseApplicationInsightsExceptionTelemetry();

            app.UseMvc();

            app.UseSwagger();

            app.UseSwaggerUi();
        }

        /// <summary>
        /// Gets the XML comments path.
        /// </summary>
        /// <returns></returns>
        private string GetXmlCommentsPath()
        {
            return string.Format(@"{0}\WWT.Feedback.API.XML", AppContext.BaseDirectory);
        }
    }
}
